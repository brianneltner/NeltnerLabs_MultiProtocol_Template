# Neltner Labs MultiProtocol Template
Library and example for creating a parser for the ESP32 to use any interface (WiFi, Ethernet, UART, SPI, etc) for message based control using an interrupt-style approach for easy addition of new functionality.

***Features:***
* Allows connections from any interface, and easily extensible to new interfaces.
* Allows connections from multiple instances of the same interface, for instance multiple UARTs.
* Implements a command registry to allow the developer to easily add new commands for automatic checking against messages received on the interface.
* Interrupt-style programming, define a handler function and it is automatically called if a received message matches the command specified in the registry.
* Strict typechecking for arguments, handler returns immediately if the user sends invalid arguments.
* Implements a reply() function to allow your handler to send responses to the same interface the command was received on.
* Highly defensive design intended to check for number of arguments, type of arguments, and then bounds checking prior to carrying out a received command.
* Utilize non-volatile storage to allow the user to store a ssid and password, set them remotely over Serial (or WiFi if you're already connected), and save them to the device for use on the next reboot.

***Current Major Issues:***
* ESP32 does not properly close an incoming TCP/IP socket if the computer attached disconnects. I suspect a bug in the library, but it is an open issue right now. At present, the device needs to be rebooted if the TCP/IP socket is disconnected (Serial continues to work fine).

***Enhancements in the Works:***
* ESP32 seems to have no way to attempt to connect to a network if WiFi.begin() has already been run. If I can find a way to do something like WiFi.reconnect(ssid, password) I can configure it to automatically attempt to reconnect if the WiFi connection is lost or if the user changes the ssid and password.
* Currently I've only implemented WiFi and Serial, but should add options for Ethernet and SPI at least. Adding support involves simply implementing the command to check a certain type of port for messages and adding the new interfaces to the meta-class InterfaceClass which is used to allow a user to pass any kind of interface as an argument without type issues by using an internal variable defining the interface type based on the type of interface passed to it. Functions using the InterfaceClass object check the interface type and call the appropriate communication functions.
* Add the ability to send a string in quotations without interpreting the spaces as indicating a new argument.

## Introduction:

In general intent, this library is specifically intended to make it extremely fast to write custom firmware using message based control from a computer or other device. It avoids the need for the developer to write complex message parsing functions and makes your main code body very easy to read and evaluate for correctness to avoid common user errors as well as avoiding code issues by flattening and reducing the complexity of your parser.

The MultiProtocol template is intended to parallel the usage of interrupt handlers -- the command is automatically read, triggers an "interrupt" that calls the specified command, and the only major difference is that the commands have the special power of pulling in as variables the arguments provided with the message. It currently does not support spaces inside a string.

In design philosphy, it is designed to be **extremely defensive**. If the end user types invalid commands, it should not execute any code other than to tell the user that their command was invalid. By example of where this is really important, say you have a command SetPurge which accepts either a 0 or a 1 to indicate on or off. If the end user doesn't read the manual you give them, and tries to do "SetPurge ON", the normal string.toInt() function would return -- zero. Not an error, but zero. So you'd end up setting some valve to off when the user intended it to be on.

These errors are pernicious and hard to anticipate, and so this library works to make it so that everything is checked always and if there is anything unexpected to instead warn the end user and not execute potentially dangerously misinterpreted commands. It may not be as ideal as anticipating every user error and guessing what they meant, but it puts the burden on the end user to actually send proper commands as you expected them to be sent rather than on you to guess what they'll do wrong.

## Library Usage:

### command(name)

Creates a command function, similar to an interrupt handler, which is called by the checkSerial() command when it receives a matching serial command.

Use this by creating the section:

    command(name) {
      <Code to Execute>
    }

I recommend having these at the bottom of the code and to use a formal prototype at the top due to the added benefit of having a single compact list of all of the commands registered to the interface. But you can do it either way, it's just a function definition that needs to happen before registerCommand() in the file.

Under the hood, this creates a function named "name", so you cannot use the chosen name for any other functions. i.e. **Do not do** (strict checking deleted for clarity):

```
command(toggle) {
  int value = intArg(1);
  toggle(value);
}

void toggle(boolean value) {
  digitalWrite(1, value);
}
```

because although it looks like a function called "command" calling a function "toggle" in reality it's a function "toggle" trying to call a second defined function "toggle".

If you want to do this kind of thing where you have a helper function callable from (for instance) multiple commands, just give the helper function a different name from any command.

**Do do this -- different name for helper function than any command:**
```
command(toggle) {
  int pin = intArg(1);
  int value = intArg(2);
  setPin(pin, value);
}

command(multitoggle) {
  int NumberOfArguments = numArgs();
  int pins[NumberOfArgument/2];
  int values[NumberOfArguments/2];

  for (int i = 1; i <= NumberOfArguments/2; i=i+2) {
      pins[i] = intArg(i);
      values[i] = intArg(i+1);
  }

  // And then actually use them.
  for (int i = 0; i < NumberOfArguments/2; i++) setPin(pins[i], values[i]);
}

void setPin(int pin, int Value) {
  digitalWrite(pin, value);
}
```

Observe also that I demonstrate how to pull all the arguments into the function through the parser before attempting to work with them. In this example, if any of the arguments **aren't** valid ints, it will return an error saying that the offending argument was invalid whereas if you just did:

    setPin(intArg(1), intArg(2));
    setPin(intArg(3), intArg(4));

it would return immediately when intArg() finds an invalid value, but if the invalid value was argument 3 then it will have already set one of the pins before noticing. I think in most cases the behavior where none of the actual function work is done unless *all* arguments used are valid first is preferred to prevent end user errors that are hard to predict from breaking anything important.

#### Special Functions inside of a command block

While inside the command(name) { } function definition, there are five special functions which you can use to populate local variables from the arguments in the command string.

***NOTE:*** These functions will not work in functions called from the handler. If you have a function called by your handler, you must first import all of the variables and pass them as normal. If you wish to use the reply() function, you will have to have your function return a string to pass to reply() from inside your handler.

##### float floatArg(int i)

Converts the i-th argument into a float (if valid) and returns. For an invalid conversion, this special function will break out of the main command, so do this at the beginning of a command function to check all of the variable conversions before doing work on them. It checks that the argument number is valid and is a valid float. Invalid floats or argument numbers cause the function to return immediately, printing error messages to the Serial port for debugging either the code error or the command error.

##### int intArg(int i)

Converts the i-th argument into an int (if valid) and returns. For an invalid conversion, this special function will break out of the main command, so do this at the beginning of a command function to check all of the variable conversions before doing work on them. It checks that the argument number is valid and is a valid float. Invalid ints or argument numbers cause the function to return immediately, printing error messages to the Serial port for debugging either the code error or the command error.

##### string stringArg(int i)

Converts the i-th argument into a string (if valid) and returns. For an invalid conversion, this special function will break out of the main command, so do this at the beginning of a command function to check all of the variable conversions before doing work on them. It checks that the argument number is valid, but all arguments start as valid string objects so there is no possibility of an invalid string to string conversion. Invalid argument numbers cause the function to return immediately, printing error messages to the Serial port for debugging either the code error or the command error.

##### int numArgs()

Returns the total number of arguments, not including the command itself. i.e. if you send "SetPurge 1" over serial, and then in the registered function you ask numArgs() it will return 1. This allows you to check for a fully properly formatted string with no extra parameters that shouldn't be there.

##### reply()

This function takes a string and sends it out over whichever interface the command was received on.

### void registerCommand(string name, function* name, string description);

In the setup section, you must register each command with (for example):

    registerCommand("CommandName", commandname, "Does a commanding Thing")
   
with previously defined:

    command(commandname) { };

This automatically adds the new Command to a vector of Commands. This is your command registry, and is automatically searched for a match to a command name if a serial packet with a terminal newline or carriage return is present when you run (for example)

    checkSerial(&Serial);
    checkWiFi(&client);

Upon a match, checkSerial() and checkWiFi() automatically runs the registered function associated with the name with the correct interface recorded for reply().

### void checkSerial(HardwareSerial*)
### void checkWiFi(WiFiClient*)

This checks interfaces for new bytes, and each function requires as an argument a valid existing interface instance to tell it which one to use.

If there is new data it adds it to a buffer. If it sees a carriage return or newline, it takes the data so far and evaluates it against the registered functions. This should be run with high priority inside the main loop since otherwise the Serial buffer will just fill up instead of being parsed.

The above routine has the advantage of allowing for either newline or carriage returns as a termination character, frequently a point of contention when using LabView or other very high level GUI systems, and allows for you to use backspace to delete typed characters. The test against 0x20 and 0x7E tells it to only store "normal" characters.

If you send the special serial command "ListCommands" it will print back a list of all registered commands along with the descriptions provided when registering. "Help", "?" and "help" also print a list of registered functions. For instance, in you had registered CommandName with:

    registerCommand("CommandName", commandname, "Does a commanding Thing")

sending ListCommands over serial would print:

    ListCommands
    CommandName - Does a commanding Thing

If there is no match for a registered function when it is received in a message, it prints back:

    MissingCommand
    ERROR - No such command - MissingCommand