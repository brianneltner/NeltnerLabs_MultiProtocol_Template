#pragma once

#include <Arduino.h>
#include <errno.h>
#include <limits.h>
#include <vector>
#include <WiFi.h>

enum interfacetype {
    INTERFACE_SERIAL,
    INTERFACE_WIFI,
    INTERFACE_ETHERNET
};

class InterfaceClass {
    private:
        interfacetype _source;
        WiFiClient* _wifiinterface;
        HardwareSerial* _serialinterface;
    public:
        InterfaceClass(WiFiClient* wifiinterface);
        InterfaceClass(HardwareSerial* serialinterface);
        interfacetype getType(void);
        WiFiClient* getWiFi(void);
        HardwareSerial* getSerial(void);
};

#define floatArg(i)  ({ float var_ptr; if (-1 == _parse_float(argv, i, &var_ptr)) return "Parser Failed."; var_ptr; })
#define intArg(i)  ({ int var_ptr; if (-1 == _parse_int(argv, i, &var_ptr)) return "Parser Failed."; var_ptr; })
#define stringArg(i)  ({ String var_ptr; if (-1 == _parse_string(argv, i, &var_ptr)) return "Parser Failed."; var_ptr; })
#define numArgs() (argv.size()-1)

#define reply(message) ({ if (interface.getType() == INTERFACE_SERIAL) { HardwareSerial* port = interface.getSerial(); _reply(message, port); }; if (interface.getType() == INTERFACE_WIFI) { WiFiClient* port = interface.getWiFi(); _reply(message, port); }; 0; })

#define command(name) static String name(std::vector<String> argv, InterfaceClass& interface)

typedef String (handler_t)(std::vector<String> argv, InterfaceClass& interface);

int _parse_float(std::vector<String> argv, int i, float *flt);
int _parse_int(std::vector<String> argv, int i, int *num);
int _parse_string(std::vector<String> argv, int i, String *str);
void registerCommand(String name, handler_t *handler, String description);
void checkSerial(HardwareSerial& interface);
int checkWiFi(WiFiClient& interface);

void _reply(String message, WiFiClient* interface);
void _reply(String message, HardwareSerial* interface);