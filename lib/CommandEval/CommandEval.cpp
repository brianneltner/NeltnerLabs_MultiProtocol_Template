#include "CommandEval.h"

InterfaceClass::InterfaceClass(WiFiClient* wifiinterface):
  _source(INTERFACE_WIFI),
  _wifiinterface(wifiinterface) {};

InterfaceClass::InterfaceClass(HardwareSerial* serialinterface):
  _source(INTERFACE_SERIAL),
  _serialinterface(serialinterface) {};

WiFiClient* InterfaceClass::getWiFi(void) {
  if (getType() == INTERFACE_WIFI) return _wifiinterface;
  return NULL;
}

HardwareSerial* InterfaceClass::getSerial(void) {
  if (getType() == INTERFACE_SERIAL) return _serialinterface;
  return NULL;
}

interfacetype InterfaceClass::getType(void) {
  return _source;
}

struct Command {
  String name;
  String description;
  handler_t *handler;
};

std::vector<Command> CommandVector;

void evaluateCommand(String commandstring, InterfaceClass& interface) {
  // If commandstring is empty, return without doing anything. No point in
  // evaluating an empty command.
  
  if (commandstring.length() == 0) return;
  
  std::vector<String> parsed_argv;

  // Get the index of the first space. This should be after the command name.
  int spaceIndex = commandstring.indexOf(' ');

  // Until there are no more spaces, keep adding the char* from the commandstring
  // as new elements of the char* vector parsed_argv.
  while (spaceIndex != -1) {
    parsed_argv.push_back(commandstring.substring(0, spaceIndex));
    commandstring = commandstring.substring(spaceIndex + 1);
    spaceIndex = commandstring.indexOf(' ');
  }

  // This either pushes the very last portion of the command into the final argv,
  // or in the case where there were no spaces in the command just puts the full
  // command into argv[0].
  
  parsed_argv.push_back(commandstring);

  // Flag to identify if the command was handled.
  
  boolean CommandHandled = false;

  // After parsing the string into parsed_argv, search the CommandVector of registered
  // commands for a match between the Command name and the argv[0] string. First check
  // for the special command "ListCommands".
  
  if (parsed_argv[0] == "ListCommands" or parsed_argv[0] == "Help" or parsed_argv[0] == "?" or parsed_argv[0] == "help") {    for (unsigned int i=0; i<CommandVector.size(); i++) {
      reply(CommandVector[i].name + " - " + CommandVector[i].description);
      CommandHandled = true;
    }
  }
  
  else {
    for (unsigned int i=0; i<CommandVector.size(); i++) {
      if (CommandVector[i].name == parsed_argv[0]) {
        reply(CommandVector[i].handler(parsed_argv, interface));
        CommandHandled = true;
      }
    }
  }
  
  if (!CommandHandled) reply("ERROR: No such command - " + parsed_argv[0]);
}

void registerCommand(String name, handler_t *handler, String description) {
  Command newCommand;
  newCommand.name = name;
  newCommand.handler = handler;
  newCommand.description = description;
  CommandVector.push_back(newCommand);
}

std::vector<char> serialstring;
std::vector<char> wifistring;

void checkSerial(HardwareSerial& interface) {
  while (interface.available() > 0) {
    char incomingByte = interface.read();
    
    // If the byte is a carriage return or newline (since I can't guarantee which if either will come
    // first), then send the command previously read to evaluateCommand() and clear the commandstring
    // buffer. This has the convenient effect of rendering irrelevant whether LabView or other such
    // GUI sends something reasonable for a termination character, as long as it sends *something*.
    
    if ((incomingByte == 0x0D) || (incomingByte == 0x0A)) {

      // This tests that there's a string to return in the buffer. If not, ignore. This is both
      // to avoid testing when it's an empty command, and also to deal with sequential CR/NL that
      // can happen with some GUIs sending serial data.
      
      if (serialstring.size() != 0) {
        // Append the termination character to the command string.
        serialstring.push_back('\0');
  
        // Write a newline to clean up echoing.
        Serial.write(0x0D);
        Serial.write(0x0A);
  
        // Evaluate the data.
        InterfaceClass port = InterfaceClass(&interface);
        evaluateCommand(serialstring.data(), port);
      }
      serialstring.clear();
    }

    // If the byte is a backspace, remove the previously appended char if the length is non-zero.
    else if (incomingByte == 0x7F) {
      if (serialstring.size() > 0) {
        serialstring.pop_back();
        interface.write(0x7F);
      }
    }
    
    // If the byte is not a carriage return, and is a normal ASCII character, put it onto the commandstring.
    else if ((incomingByte >= 0x20) && (incomingByte <=0x7E)) {
      serialstring.push_back(incomingByte);
      interface.write(incomingByte);
    }
  }
}

int checkWiFi(WiFiClient& interface) {
  if (interface.connected()) {
    while (interface.available() > 0) {
      char incomingByte = interface.read();
      
      // If the byte is a carriage return or newline (since I can't guarantee which if either will come
      // first), then send the command previously read to evaluateCommand() and clear the commandstring
      // buffer. This has the convenient effect of rendering irrelevant whether LabView or other such
      // GUI sends something reasonable for a termination character, as long as it sends *something*.
      
      if ((incomingByte == 0x0D) || (incomingByte == 0x0A)) {
        // This tests that there's a string to return in the buffer. If not, ignore. This is both
        // to avoid testing when it's an empty command, and also to deal with sequential CR/NL that
        // can happen with some GUIs sending serial data.
        
        if (wifistring.size() != 0) {
          // Append the termination character to the command string.
          wifistring.push_back('\0');
    
          // Evaluate the data.
          InterfaceClass port = InterfaceClass(&interface);
          evaluateCommand(wifistring.data(), port);
        }
        wifistring.clear();
      }

      // If the byte is not a carriage return, and is a normal ASCII character, put it onto the commandstring.
      else if ((incomingByte >= 0x20) && (incomingByte <=0x7E)) {
        wifistring.push_back(incomingByte);
      }
    }
    return 0;
  }
  else return 1;
}

int _parse_float(std::vector<String> argv, int paramnum, float *flt) {
  if (paramnum < 0) {
    Serial.println("ERROR: In Function Call, invalid argument number.");
    return -1;
  }
  unsigned int i = paramnum;
  if (!(0 <= i && i < argv.size())) {
    Serial.println("ERROR: Missing argument " + String(i) + ".");
    return -1;
  }
  char *end;
  *flt = strtof(argv[i].c_str(), &end);
  if (errno == ERANGE) {
    Serial.println("ERROR: Argument " + String(i) + " - (float) not in range.");
    return -1;
  }
  if (end[0] != '\0') {
    Serial.println("ERROR: Argument " + String(i) + " - invalid (float).");
    return -1;
  }
  return 0;
}

int _parse_int(std::vector<String> argv, int paramnum, int *num) {
  if (paramnum < 0) {
    Serial.println("ERROR: In Function Call, invalid argument number.");
    return -1;
  }
  unsigned int i = paramnum;
  if (!(0 <= i && i < argv.size())) {
    Serial.println("ERROR: Missing argument " + String(i) + ".");
    return -1;
  }
  char *end;
  long numl = strtol(argv[i].c_str(), &end, 0);
  *num = numl;
  if (errno == ERANGE || numl > INT_MAX || numl < INT_MIN) {
    Serial.println("ERROR: Argument " + String(i) + " - (int) not in range.");
    return -1;
  }
  if (end[0] != '\0') {
    Serial.println("ERROR: Argument " + String(i) + " - invalid (int).");
    return -1;
  }
  return 0;
}

int _parse_string(std::vector<String> argv, int paramnum, String *str) {
  if (paramnum < 0) {
    Serial.println("ERROR: In Function Call, invalid argument number.");
    return -1;
  }
  unsigned int i = paramnum;
  if (!(0 <= i && i < argv.size())) {
    Serial.println("ERROR: Missing argument " + String(i) + ".");
    return -1;
  }
  *str = argv[i];
  return 0;
}

void _reply(String message, HardwareSerial* interface) {
  interface->println(message);
}

void _reply(String message, WiFiClient* interface) {
  if (interface->connected()) interface->println(message);
}